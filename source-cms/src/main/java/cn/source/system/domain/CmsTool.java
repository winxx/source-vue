package cn.source.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.source.common.annotation.Excel;
import cn.source.common.core.domain.BaseEntity;

/**
 * 工具管理对象 cms_tool
 * 
 * @author sourcebyte.vip
 * @date 2024-05-02
 */
public class CmsTool extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 工具名称 */
    @Excel(name = "工具名称")
    private String name;

    /** 工具介绍 */
    @Excel(name = "工具介绍")
    private String intro;

    /** 工具缩略图 */
    @Excel(name = "工具缩略图")
    private String thumb;

    /** 工具地址 */
    @Excel(name = "工具地址")
    private String url;

    /** 提取密码 */
    @Excel(name = "提取密码")
    private String extractPassword;

    /** 工具内容 */
    @Excel(name = "工具内容")
    private String content;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setIntro(String intro) 
    {
        this.intro = intro;
    }

    public String getIntro() 
    {
        return intro;
    }
    public void setThumb(String thumb) 
    {
        this.thumb = thumb;
    }

    public String getThumb() 
    {
        return thumb;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setExtractPassword(String extractPassword) 
    {
        this.extractPassword = extractPassword;
    }

    public String getExtractPassword() 
    {
        return extractPassword;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("intro", getIntro())
            .append("thumb", getThumb())
            .append("url", getUrl())
            .append("extractPassword", getExtractPassword())
            .append("content", getContent())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .toString();
    }
}
